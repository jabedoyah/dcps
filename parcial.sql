-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-04-2015 a las 19:15:29
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `work`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actor`
--

CREATE TABLE IF NOT EXISTS `actor` (
  `cedula` bigint(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  PRIMARY KEY (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actor`
--

INSERT INTO `actor` (`cedula`, `nombre`, `apellido`) VALUES
(1, 'Juan', 'Alvarez'),
(2, 'Laura', 'Lopez'),
(3, 'Cristian', 'Parra'),
(4, 'Andres', 'Mona');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actuacion`
--

CREATE TABLE IF NOT EXISTS `actuacion` (
  `cod` bigint(20) NOT NULL,
  `personaje` varchar(20) NOT NULL,
  `pelicula` bigint(20) NOT NULL,
  `actor` bigint(20) NOT NULL,
  PRIMARY KEY (`cod`,`pelicula`,`actor`),
  KEY `p_a` (`pelicula`),
  KEY `a_a` (`actor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actuacion`
--

INSERT INTO `actuacion` (`cod`, `personaje`, `pelicula`, `actor`) VALUES
(199, 'villano', 100, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE IF NOT EXISTS `pelicula` (
  `cod` bigint(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `genero` varchar(30) NOT NULL,
  `puntaje` int(4) NOT NULL,
  `idioma` varchar(20) NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`cod`, `nombre`, `genero`, `puntaje`, `idioma`) VALUES
(100, 'velocidad', 'accion', 3, 'ingles'),
(200, 'amor', 'romantica', 2, 'espanol');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actuacion`
--
ALTER TABLE `actuacion`
  ADD CONSTRAINT `p_a` FOREIGN KEY (`pelicula`) REFERENCES `pelicula` (`cod`),
  ADD CONSTRAINT `a_a` FOREIGN KEY (`actor`) REFERENCES `actor` (`cedula`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
