<?php
    class profesor extends object_standard {
        
        //attribute variables
        protected $cedula;
        protected $nombre;
        protected $fecha;
        protected $lugar_nacimiento;
        protected $titulo;
        protected $departamento;

        //components
        var $components = array();

        //auxiliars for primary key and for files
        var $auxiliars = array();

        //data about the attributes
        public function metadata() {
            return array("cedula" => array(), "nombre" => array(), "fecha" => array(), "lugar_nacimiento" => array(), "titulo" => array(), "departamento" => array());
        }

        public function primary_key() {
            return array("cedula");
        }

        public function relational_keys($class, $rel_name) {
            switch($class) {
                default:
                break;
            }
        }
    }
?>