<?php
require('configs/include.php');
class c_registrar_profesor extends super_controller{
    public function add(){
        $profesor = new profesor($this->post);
        if(is_empty($profesor->get('cedula'))){
        throw_exception("Debe ingresar una cédula");}
        
        $this->orm->connect();
        $this->orm->insert_data("normal",$profesor);
        $this->orm->close();
        
        $this->type_warning = "success";
        $this->msg_warning = "Profesor agregado correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning', $this->type_warning);
        $this->engine->assign('msg_warning', $this->msg_warning);   
    }
    
    public function display(){
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('registrar_profesor.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run(){
        try {if (isset($this->get->option)){$this->{$this->get->option}();}}
        catch (Exception $e){
            $this->error = 1; 
            $this->engine->assign('object', $this->post);
            $this->msg_warning = $e->getMessage();
            $this->engine->assign('type_warning', $this->type_warning);
            $this->engine->assign('msg_warning', $this->msg_warning);
            $this->temp_aux = 'message.tpl';
        }
        $this->display();
    }
}

$call = new c_registrar_profesor();
$call->run();
?>

