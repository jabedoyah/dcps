<?php
require('configs/include.php');

class c_lista_profesor extends super_controller{
    
    public function display(){
        $options['profesor']['lvl2']='all';
        $this->orm->connect();
        $this->orm->read_data(array("profesor"),$options);
        $profesor = $this->orm->get_objects("profesor");
        $this->orm->close();
        
        $this->engine->assign('profesor',$profesor);
        $this->engine->assign('title','Extraer profesor');
        
        $this->engine->display('header.tpl');
        $this->engine->display('lista_profesor.tpl');
        //$this->engine->display('lista_granjeros_c.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run(){
        $this->display();
    }
}

$call = new c_lista_profesor();
$call->run();
?>