<?php
require('configs/include.php');
class c_editar_profesor extends super_controller{
    
    public function select(){
        
        $cod['profesor']['cedula'] = $this->post->cedula;
        $options['profesor']['lvl2'] = 'one';
        $this->orm->connect();
        $this->orm->read_data(array('profesor'), $options, $cod);
        $profesor = $this->orm->get_objects("profesor");
        
        if(!isset($profesor[0])){
            throw_exception("Debe ingresar un id válido");
        }
        
        $this->temp_aux2='editar_profesor_c.tpl';
        $this->engine->assign('profesor',$profesor);
        $this->orm->close();
        
    }
    
    public function upd(){

        $profesor = new profesor($this->post);
	$profesor->auxiliars['cedula_old'] = $this->post->cedula_old;
        
        $this->orm->connect();
        $this->orm->update_data("normal",$profesor);
        $this->orm->close();
        
        $this->type_warning = "success";
        $this->msg_warning = "Profesor editado correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning', $this->type_warning);
        $this->engine->assign('msg_warning', $this->msg_warning); 
    }
    
    public function display(){
        
        $this->engine->assign('title', 'Editar profesor');
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        
        if ($this->temp_aux2 != 'empty.tpl'){
            $this->engine->display($this->temp_aux2);
	}else{
            $this->engine->display('editar_profesor.tpl');
	}
        
        $this->engine->display('footer.tpl');
    }
    
    public function run(){
        
        try {if (isset($this->get->option)){$this->{$this->get->option}();}}
        catch (Exception $e){
            $this->error = 1; 
            $this->engine->assign('object', $this->post);
            $this->msg_warning = $e->getMessage();
            $this->engine->assign('type_warning', $this->type_warning);
            $this->engine->assign('msg_warning', $this->msg_warning);
            $this->temp_aux = 'message.tpl';
        }
        $this->display();
    }
}

$call = new c_editar_profesor();
$call->run();
?>
