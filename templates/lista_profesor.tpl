<div class="square">

<b>Profesores</b><br/><br/>

{if isset($profesor)}
    {section loop=$profesor name=i}
    <b>Cédula: </b> {$profesor[i]->get('cedula')}<br/>
    <b>Nombre: </b> {$profesor[i]->get('nombre')}<br/>
    <b>Fecha de nacimiento: </b> {$profesor[i]->get('fecha')}<br/>
    <b>Lugar de nacimiento: </b> {$profesor[i]->get('lugar_nacimiento')}<br/>
    <b>Título: </b> {$profesor[i]->get('titulo')}<br/>
    <b>Departamento: </b> {$profesor[i]->get('departamento')}<br/><br/>
    {/section}
    {/if}

</div>